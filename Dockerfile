FROM golang:1-alpine as builder
LABEL maintainer="ulrich.schreiner@gmail.com"

ARG CADDY_VERSION
ENV GO111MODULE=on

RUN apk update \
    && apk add git \
    && mkdir /go/mycaddy

COPY plugins.go /go/mycaddy/plugins.go

WORKDIR /go

RUN cd mycaddy \
    && go mod init mycaddy \
    && go get github.com/caddyserver/caddy/caddy@${CADDY_VERSION} \
    && go install

FROM alpine:3.10
LABEL maintainer="ulrich.schreiner@gmail.com"
ARG CADDY_UID
ARG CADDY_GID
RUN apk --no-cache add ca-certificates libcap && mkdir /conf
WORKDIR /srv
COPY Caddyfile /conf/Caddyfile
COPY index.html /srv/index.html
RUN addgroup -S -g ${CADDY_GID} caddy \
    && adduser -u ${CADDY_UID} -D -S -s /sbin/nologin -G caddy caddy \
    && chown -R caddy:caddy /srv
COPY --from=builder /go/bin/mycaddy /usr/bin/caddy
RUN setcap cap_net_bind_service=+ep /usr/bin/caddy
EXPOSE 80 443 2015
ENTRYPOINT ["/usr/bin/caddy"]
CMD ["--conf", "/conf/Caddyfile"]
USER caddy
