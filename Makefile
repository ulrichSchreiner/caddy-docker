.ONESHELL:

CADDY_VERSION := "v1.0.1"
CADDY_UID := 2002
CADDY_GID := 2002

.phony:
build:
	docker build -t ulrichschreiner/caddy-http:$(CADDY_VERSION) \
		--build-arg CADDY_VERSION=$(CADDY_VERSION) \
		--build-arg CADDY_GID=$(CADDY_GID) \
		--build-arg CADDY_UID=$(CADDY_UID) \
		.

.phony:
push:
	docker push ulrichschreiner/caddy-http:$(CADDY_VERSION)

.phony:
showplugins:
	docker run -it --rm ulrichschreiner/caddy-http:$(CADDY_VERSION) -plugins
